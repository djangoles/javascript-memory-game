const tableBody = document.querySelector('.table-main')
const modalContainer = document.querySelector('.modal-container')
const modalBody = document.querySelector('.modal-body')
const name = document.querySelector('.modal-body-name')
const title = document.querySelector('.modal-body-title')
const img = document.querySelector('.modal-body-img')
const bio = document.querySelector('.modal-body-bio')
const modalBodyBioText = document.querySelector('.modal-body-bio-text') 
const endGameModal = document.querySelector('.end-game-modal')
const restartGame = document.querySelector('.restart-game')

const appState = {
    boardObj: [
        {
            id:1,
            name: 'AmStaff',
            bio: 'The American Staffordshire Terrier, known to their fans as AmStaffs, are smart, confident, good-natured companions. Their courage is proverbial. A responsibly bred, well-socialized AmStaff is a loyal, trustworthy friend to the end. ',
            img: 'AmStaff.06.jpg',
            title: 'Confident, Smart, Good-Natured'
        },
        {
            id:2,
            name: 'Beagle',
            bio: 'Not only is the Beagle an excellent hunting dog and loyal companion, it is also happy-go-lucky, funny, and—thanks to its pleading expression—cute.',
            img: 'Beagle-On-White-07.jpg',
            title: 'Friendly, Curious, Merry '
        },
        {
            id:3,
            name: 'German Shepherd',
            bio: 'Generally considered dogkind’s finest all-purpose worker, the German Shepherd Dog is a large, agile, muscular dog of noble character and high intelligence.  ',
            img: 'German-Shepherd-on-White-00.jpg',
            title: 'Confident, Courageous, Smart '
        },
        {   
            id:4,
            name: 'Golden Retriever',
            bio: 'The Golden Retriever, an exuberant Scottish gundog of great beauty, stands among America’s most popular dog breeds. They are serious workers at hunting and field work, as guides for the blind, and in search-and-rescue.',
            img: 'Golden-Retriever-On-White-05.jpg',
            title: 'Friendly, Intelligent, Devoted'
        },
        {
            id:5,
            name: 'Mastiff',
            bio: 'The colossal Mastiff belongs to a canine clan as ancient as civilization itself. A massive, heavy-boned dog of courage and prodigious strength, the Mastiff is docile and dignified but also a formidable protector of those they hold dear.',
            img: `Mastiff-On-White-03.jpg`,
            title: 'Brave, Dignified, Good-Natured'
        },
        {
            id:6,
            name: 'Pomeranian',
            bio: 'The tiny Pomeranian, long a favorite of royals and commoners alike, has been called the ideal companion. The glorious coat, smiling, foxy face, and vivacious personality have helped make the Pom one of the world\'s most popular toy breeds.',
            img: 'Pomeranian-On-White-01.jpg',
            title: 'Inquisitive, Bold, Lively'
        },
        {
            id:7,
            name: 'Welsh Terrier',
            bio: 'The Welsh Terrier is as alert and spirited as any self-respecting terrier, but a bit calmer than most—“game, not quarrelsome,” as breed fanciers say. The Welshman was bred to do battle with badgers, otters, and other dangerous opponents.',
            img: 'Welsh-Terrier-On-White-03.jpg',
            title: 'Friendly, Spirited, Intelligent '
        },
        {
            id:8,
            name: 'Dalmatian',
            bio: `The dignified Dalmatian, dogdom's citizen of the world, is famed for his spotted coat and unique job description. During their long history, these "coach dogs" have accompanied many.`,
            img: 'Dalmatian-On-White-01.jpg',
            title: 'Dignified, Smart, Outgoing'
        },
    ],
    matchedCount: 0,
    clickCount: 0,
    clickChoices: [],
    savedColors:[]
}

const createUIBoard = (row, col) => {
    const randomArrayIndexes =  getRandomArrayIndexes(16)
    const board1 = [...appState.boardObj];
    const board2 = [...appState.boardObj];
    let newBoard = board1.concat(board2)
    let randIdxCounter = 0
    for (var rowindex = 0; rowindex < row; rowindex++) {    
        const tr = document.createElement('tr')
        tr.classList.add('table-row')
        for (let colindex = 0; colindex < col; colindex++) {
            const td = document.createElement('td') 
            td.addEventListener('click', handleClick)
            td.classList.add('table-data')
            td.boxData = newBoard[randomArrayIndexes[randIdxCounter]]
            td.dataset.boxData = JSON.stringify(newBoard[randomArrayIndexes[randIdxCounter]])
            td.style.background = '#eee'  
            randIdxCounter++
            td.classList.add('table-data-show')
            tr.appendChild(td)
        }
        tableBody.appendChild(tr)
    }
}

const getRandomArrayIndexes = (num) =>{
    let counter = 0;
    let randomArray = [];
    while(counter < num) {
        const randomIndex = getRandomNum(num)
        if(!randomArray.includes(randomIndex)) {
            randomArray.push(randomIndex)
            counter++
        }
    }
    return randomArray
}

const getRandomNum = (num) => {
    return Math.floor(Math.random() * num)
}

const getRandomRGBColor = () => {
    return `rgb(${getRandomNum(150)}, ${getRandomNum(150)}, ${getRandomNum(150)})`
}

let typecounter = 0

const startTyping = (elem, text) => {

}

const handleClick = (e) => {
    appState.clickCount++
    let elm = e.target 
    elm.removeEventListener('click', handleClick)
    
    // STATE UPDATE =============================
    appState.clickChoices.unshift(elm.boxData)
    // UI UPDATE =================================
    appState.savedColors.push(elm.style.background)
    elm.style.background = ''
    elm.style.backgroundImage =`url(img/${appState.clickChoices[0].img})`
    elm.style.backgroundSize =`100px` 

    // GET ALL SQUARES
    let squares = document.querySelectorAll('.table-data')

    // STATE UPDATE WITH CHOICES=========================
    const choice1 = appState.clickChoices[0]
    const choice2 = appState.clickChoices[1]
    if(appState.clickCount === 2) {
        // RUN CHECK HERE ======================
        if(choice1.id === choice2.id) {
            appState.matchedCount++
            name.textContent = choice1.name
            title.textContent = choice1.title
            modalBodyBioText.textContent = ''
            modalBody.style.backgroundColor = ''
            modalBody.style.backgroundImage = `url(img/${choice1.img})`
            modalBody.style.backgroundSize =`300px` 
            setTimeout(() => {
                modalContainer.classList.toggle('modal-container-show')
                tableBody.classList.toggle('table-main-hide')
            }, 500)

            let buildString = ''

            setTimeout(() => {
                modalBodyBioText.textContent = ""  
                let pleaseWork = setInterval(() => {
                    if(typecounter === choice1.bio.length) {
                        window.clearInterval(pleaseWork);
                        typecounter = 0
                    } else {
                        buildString+= choice1.bio[typecounter]
                        modalBodyBioText.textContent = buildString
                        typecounter++ 
                    }
                }, 50)


            }, 1000)

        } else {
            // REMOVE PIC AND BRING BACK COLOR BG 
            squares.forEach((square, i, arr) => {
                if(square.boxData.id === choice1.id  ) {
                    setTimeout(()=>{
                        square.style.background = appState.savedColors[0]
                        square.style.backgroundImage =``
                        elm.style.backgroundSize =`100px`
                        square.addEventListener('click',handleClick)
                    },500)
                }
                if(square.boxData.id === choice2.id ) {
                    setTimeout(()=>{
                        square.style.background = appState.savedColors[1]
                        square.style.backgroundImage =``
                        elm.style.backgroundSize =`100px`
                        square.addEventListener('click',handleClick)
                    },500)
                }
            });  
        }
        // RESET COUNTER & PLAY STATE
        appState.clickCount = 0
        appState.clickChoices = []
    } 
}

createUIBoard(4, 4)

modalContainer.addEventListener('click', ()=> {
    if(appState.matchedCount === appState.boardObj.length) {
        endGameModal.classList.add('end-game-modal-show')
        modalContainer.classList.toggle('modal-container-show')
        tableBody.classList.toggle('table-main-hide')
    } else {
        modalContainer.classList.toggle('modal-container-show')
        tableBody.classList.toggle('table-main-hide')
    }
})

restartGame.addEventListener('click', () => {
    document.location.reload()
})

